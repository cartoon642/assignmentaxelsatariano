package mt.edu.mcast.asyncexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

public class account extends AppCompatActivity {
    public FirebaseAuth mAuth;
    ArrayList<String> array = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        mAuth = FirebaseAuth.getInstance();


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        DatabaseReference usersRef = myRef.child("users");





        array.clear();
        usersRef.child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> items = dataSnapshot.getChildren().iterator();

                while(items.hasNext()){
                    DataSnapshot item = items.next();
                    array.add(item.getKey().toString());
                }
                showdata(array);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });


        Button btn = findViewById(R.id.buttoncoin);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt = findViewById(R.id.coinFav);


                storedata(txt.getText().toString());

            }

        });
    }
    public void storedata(String txt) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        DatabaseReference usersRef = myRef.child("users");
        DatabaseReference coinref  = usersRef.child(mAuth.getCurrentUser().getUid());
        coinref.child(txt).setValue(true);
       array.clear();
        usersRef.child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> items = dataSnapshot.getChildren().iterator();

                while(items.hasNext()){
                    DataSnapshot item = items.next();
                    array.add(item.getKey().toString());
                }
                showdata(array);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private void showdata(ArrayList<String> array) {
        int a = array.size();
        TextView txt = findViewById(R.id.coin1);
        TextView txt2 = findViewById(R.id.coin2);
        TextView txt3 = findViewById(R.id.coin3);
        if (a == 0){
            return;
        }
        if(a == 1 || a == 2 || a == 3) {

            txt.setText(array.get(0));
            AsyncTask task = new AsyncTask();
            try {
                double temp = task.execute(txt.getText().toString()).get();

                TextView tv = (TextView) findViewById(R.id.Price1);
                tv.setText("One " + txt.getText() + " = " +temp+ " USD");


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        if (a == 2 || a == 3) {

            txt2.setText(array.get(1));
            AsyncTask task = new AsyncTask();
            try {
                double temp = task.execute(txt2.getText().toString()).get();

                TextView tv = (TextView) findViewById(R.id.price2);
                tv.setText("One " + txt2.getText() + " = " +temp+ " USD");


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        if (a == 3) {

            txt3.setText(array.get(2));
            AsyncTask task = new AsyncTask();
            try {
                double temp = task.execute(txt3.getText().toString()).get();

                TextView tv = (TextView) findViewById(R.id.price3);
                tv.setText("One " + txt3.getText() + " = " +temp+ " USD");


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }





    }
    public void menu(View v) {

        Intent i = new Intent(getApplicationContext(), Menu.class);
        startActivity(i);
    }



}
