package mt.edu.mcast.asyncexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by James on 18/04/2018.
 */

public class CoinAdapter extends ArrayAdapter<Coin> {

    public CoinAdapter(Context context, ArrayList<Coin> users) {

        super(context, 0, users);

    }



    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position

        Coin coin = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.coinlist, parent, false);

        }

        // Lookup view for data population



        TextView tvUName = (TextView) convertView.findViewById(R.id.coin);

        TextView tvEmail = (TextView) convertView.findViewById(R.id.price);

        // Populate the data into the template view using the data object



        tvUName.setText(coin.getCoin());

        tvEmail.setText(coin.getPrice());

        // Return the completed view to render on screen

        return convertView;

    }

}