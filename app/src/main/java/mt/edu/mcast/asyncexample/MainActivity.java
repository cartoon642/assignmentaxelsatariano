package mt.edu.mcast.asyncexample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {


    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView lv = findViewById(R.id.listView);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        CoinAdapter userAdapter = new CoinAdapter(this, new ArrayList<Coin>());

        lv.setAdapter(userAdapter);

        CoinAsyncTask uTask = new CoinAsyncTask(userAdapter);
        uTask.execute();

    }
    public void menu(View v) {

        Intent i = new Intent(getApplicationContext(), Menu.class);
        startActivity(i);
    }

    public void onClickGetValue(View v) {

        EditText editText = (EditText) findViewById(R.id.editTextCrypto);
        String currency = editText.getText().toString();

        AsyncTask task = new AsyncTask();
        try {
            double price = task.execute(currency).get();

            TextView tv = (TextView) findViewById(R.id.CryptoView);
            tv.setText("One " + currency + " = " +price+ " USD");


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    protected void onPause() {
        super.onPause();
        edit = prefs.edit();
        Date date = new Date(System.currentTimeMillis());

        edit.putLong("time", date.getTime()).apply();
        edit.apply();
        edit.commit();

    }

    protected void onStop() {
        edit = prefs.edit();
        super.onStop();
        Date date = new Date(System.currentTimeMillis());

        edit.putLong("time", date.getTime()).apply();
        edit.apply();
        edit.commit();

    }

    protected void onStart() {
        super.onStart();


        Date date = new Date(prefs.getLong("time", 0));


        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), date.toString() , Snackbar.LENGTH_LONG);
        snackbar.show();

    }
    protected void onResume() {
        super.onResume();


        Date date = new Date(prefs.getLong("time", 0));


        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), date.toString() , Snackbar.LENGTH_LONG);
        snackbar.show();

    }



}
