package mt.edu.mcast.asyncexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;

public class Calculator extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);


    }
    public void onClickCalculate(View v) {

        TextView calculation = (TextView) findViewById(R.id.showCalc);
        EditText dollar = findViewById(R.id.txtDollar);
        EditText currency = findViewById(R.id.txtCurrency);

        AsyncTask task = new AsyncTask();
        try {
            double coin = task.execute(currency.getText().toString()).get();

            double dollarprice = Double.parseDouble(dollar.getText().toString());

            calculation.setText(dollar.getText().toString()+ " Dollars = " + Double.toString(dollarprice/coin)+ " " +currency.getText().toString());


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
    public void menu(View v) {

        Intent i = new Intent(getApplicationContext(), Menu.class);
        startActivity(i);
    }
}
