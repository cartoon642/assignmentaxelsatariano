package mt.edu.mcast.asyncexample;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by James on 18/04/2018.
 */

public class JSONParser {

    public static ArrayList<Coin> getUserList(String data) throws JSONException {

        ArrayList<Coin> userList = new ArrayList<>();
        final JSONParser jParser = new JSONParser();
        JSONObject user = new JSONObject(data);
        JSONObject rates = user.getJSONObject("rates");
        Iterator<String> keys = rates.keys();
        while (keys.hasNext()) {
            String key = keys.next();

                userList.add(new Coin(key, rates.get(key).toString()));




        }
        return userList;
    }
}
