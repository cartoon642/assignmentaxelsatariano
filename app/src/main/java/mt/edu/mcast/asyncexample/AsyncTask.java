package mt.edu.mcast.asyncexample;

import org.json.JSONException;

/**
 * Created by James on 18/04/2016.
 */
public class AsyncTask extends android.os.AsyncTask<String, Void, Double> {

    @Override
    protected Double doInBackground(String... params) {

        String data = ( (new HttpClient2()).getWeatherData());

        double Value = 0.0f;

        try {
            Value = JSONParser2.getWeather(data,params[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Value;

    }






}