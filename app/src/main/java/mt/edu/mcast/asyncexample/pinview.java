package mt.edu.mcast.asyncexample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class pinview extends AppCompatActivity

{

    Pinview p;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();


       String pincode = prefs.getString("pincode", null);

        if (pincode != null){
            TextView v = findViewById(R.id.pinView);
                v.setText("Enter Pin");

        }

        p = (Pinview)findViewById(R.id.mypinview);
        p.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                String pincode = prefs.getString("pincode", null);


                if (pincode != null){
                    if (pincode.equals(pinview.getValue())){
                        startlogin();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Wrong Pin", Toast.LENGTH_LONG).show();
                    }

                }
                if (pincode == null) {


                    pincode = prefs.getString("pincode", null);
                    //Make api calls here or what not
                    Toast.makeText(getApplicationContext(), pinview.getValue(), Toast.LENGTH_SHORT).show();

                    String i = pinview.getValue();
                    edit.putString("pincode", i);
                    edit.apply();
                    edit.commit();
                    startlogin();
                }
            }
        });


    }
    public void startlogin(){
        Intent i = new Intent(getApplicationContext(), Login.class);
        startActivity(i);
    }
}
