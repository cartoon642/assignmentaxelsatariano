package mt.edu.mcast.asyncexample;


/**
 * Created by James on 18/04/2016.
 */
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser2 {

    public static double getWeather(String data, String coin) throws JSONException  {

        JSONObject jObj = new JSONObject(data);


        JSONObject mainObj = jObj.getJSONObject("rates");
        return mainObj.getDouble(coin);

    }
}
