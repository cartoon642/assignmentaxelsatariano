package mt.edu.mcast.asyncexample;


/**
 * Created by James on 18/04/2018.
 */

public class Coin {


    private String coin;
    private String price;

    public Coin(){

        setCoin("");
        setPrice("");
    }

    public Coin(String iCoin, String iPrice){

        setCoin(iCoin);
        setPrice(iPrice);
    }






    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String toString(){
        return getCoin();
    }
}
