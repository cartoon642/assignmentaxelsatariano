package mt.edu.mcast.asyncexample;


import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import org.json.JSONException;

/**
 * Created by James on 18/04/2018.
 */

public class CoinAsyncTask extends AsyncTask<Void, Void, String> {

    ArrayAdapter lvUsers;


    public CoinAsyncTask(ArrayAdapter aa){
        lvUsers = aa;
    }
    @Override
    protected String doInBackground(Void... voids) {

        HTTPClient client = new HTTPClient();
        String data = client.getJSON("http://api.coinlayer.com/api/live?access_key=0e080d3678b7bf1db11d2e28f4e41f66");

        return data;
    }

    public void onPostExecute(String data){

        try {

            lvUsers.addAll(JSONParser.getUserList(data));
            lvUsers.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
