package mt.edu.mcast.asyncexample;



import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    final int LOGIN_ACTIVITY = 5;
    TextView tvMsg;
    Pinview p;
    public FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login2);
        mAuth = FirebaseAuth.getInstance();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);





        tvMsg = findViewById(R.id.textView2);
        Button btnregister = findViewById(R.id.button2);
        Button btnLogin = findViewById(R.id.buttonlgn);
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        FirebaseUser currentUser = mAuth.getCurrentUser();

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                EditText uname = findViewById(R.id.editText);
                EditText pass = findViewById(R.id.editText2);

                authenticate(uname.getText().toString(),pass.getText().toString());

            }

        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                EditText uname = findViewById(R.id.editText);
                EditText pass = findViewById(R.id.editText2);

                login(uname.getText().toString(),pass.getText().toString());

            }

        });


    }
    @Override
    public void onStart() {
        super.onStart();


        EditText uname = findViewById(R.id.editText);
        EditText pass = findViewById(R.id.editText2);

        String username = prefs.getString("username", null);
        String password = prefs.getString("password", null);

        uname.setText(username);
        pass.setText(password);

    }
    public void authenticate(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            Toast toast = Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT);
                            toast.show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);

                        } else {
                            // If sign in fails, display a message to the user.

                            Toast toast = Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT);
                            toast.show();

                        }

                        // ...
                    }
                });
    }
    public void login (String email,String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast toast = Toast.makeText(getApplicationContext(), "Logged In", Toast.LENGTH_SHORT);
                            toast.show();
                            Intent i = new Intent(getApplicationContext(), Menu.class);
                            startActivity(i);

                        } else {
                            // If sign in fails, display a message to the user.

                            Toast toast = Toast.makeText(getApplicationContext(), "Does not exist", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        // ...
                    }
                });
    }

    protected void onPause() {
        super.onPause();
        EditText uname = findViewById(R.id.editText);
        EditText pass = findViewById(R.id.editText2);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();

        edit.putString("username", uname.getText().toString());
        edit.apply();
        edit.commit();
        edit.putString("password", pass.getText().toString());
        edit.apply();
        edit.commit();


    }
    protected void onStop() {
        super.onStop();
        EditText uname = findViewById(R.id.editText);
        EditText pass = findViewById(R.id.editText2);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();

        edit.putString("username", uname.getText().toString());
        edit.apply();
        edit.commit();
        edit.putString("password", pass.getText().toString());
        edit.apply();
        edit.commit();


    }

    protected void onResume() {
        super.onResume();
        EditText uname = findViewById(R.id.editText);
        EditText pass = findViewById(R.id.editText2);

        String username = prefs.getString("username", null);
        String password = prefs.getString("password", null);

        uname.setText(username);
        pass.setText(password);

    }



}

